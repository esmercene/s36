const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// Create single task
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})



// Update a task
router.patch('/:id/update', (request, response) => {
	TaskController.updateTasks(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// Delete a task
router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id, request.body).then((result) => {
		response.send(`Task with id: ${request.params.id} was successfully deleted!!`)
	})
})


// Get one task
router.get('/:id', (request, response) => {
	TaskController.getTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})



// Changing the status
router.patch('/:id/complete', (request, response) => {
	TaskController.completeATask(request.params.id, request.body).then((result) => {
		response.send(`The Status of Below's Data: ${result} Was Successfully Changed!`)
	})
})
 
module.exports = router