const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
        type: String,
        new: true,
        trim: true,
        default: 'Pending'
	}
})


module.exports = mongoose.model('Task', taskSchema)