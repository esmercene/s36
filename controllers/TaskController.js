const Task = require('../models/Task.js')


// module to create a task
module.exports.createTask = (data) => {
	let newTask = new Task ({
		name: data.name
	})

	return newTask.save().then( (savedTask, error) => {
		if(error) {
			console.log(error)
			return error
		}
		return savedTask
	})
}


// module to get all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return(result)
	})
}



// module to update a task
module.exports.updateTasks = (task_Id, new_data) => {
	return Task.findById(task_Id).then((result, error) => {
		if (error){
			console.log(error)
		     return result
		    }


		result.name = new_data.name
		result.status = new_data.status

		return result.save().then((updatedTask, error) => {
			if(error) {
				console.log(error)
				return error
            }
		  return updatedTask
	   })
	})
}


// module to delete a task
module.exports.deleteTask = (task_Id) => {
	return Task.findByIdAndDelete(task_Id).then((removedTask, error) =>{
		if (error){
			console.log(error)
		     return error
		    }
		    return removedTask
           
      
	})
}
		


// module to get a specific task
module.exports.getTask = (task_Id) => {
	return Task.findById(task_Id).then((result, error) => {
		if (error){
		    console.log(error)
		     return error
		    }
		    return result
	})
}



// module to update a task
module.exports.completeATask = (task_Id, complete_data) => {
	return Task.findById(task_Id).then((result, error) => {
		if (error){
			console.log(error)
		     return result
		    }


	    
		result.status = complete_data.status
		
		return result.save().then((completedTask, error) => {
			if(error) {
				console.log(error)
				return error
            }
		  return completedTask
	   })
	})
}





