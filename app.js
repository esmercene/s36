const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoute = require('./routes/taskRoutes')

// Initialize dotenv
dotenv.config()


// Server Setup
const app = express()
const port = 3003

app.use(express.json())
app.use(express.urlencoded({extended: true}))


// Mongo DB Connection
mongoose.connect(`mongodb+srv://esmercene:${process.env.MONGODB_PASSWORD}@cluster0.t5xp9id.mongodb.net/s36-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
let db = mongoose.connection

db.on('error', () => console.error("Connection error"))
db.on('open', () => console.log("Connected to MongodDB"))


// Routes Handling
app.use('/tasks', taskRoute)



// Server Listening
app.listen(port, () => console.log(`Server Running on Localhost: ${port}`))















